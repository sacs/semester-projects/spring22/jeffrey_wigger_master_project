import multiprocessing as omp
import time
from ctypes import c_int

# from multiprocessing.queues import Queue
from multiprocessing.sharedctypes import Value

import torch.multiprocessing as mp


def do_something_out(rank, node, queue, shared):  # first argument is rank
    print(f"run {rank}")
    print(type(queue))
    node.queue.put("do_something_out")
    print(f"run {rank}")
    node.val = 2
    shared.value = 2
    print("run")


class Node:
    def __init__(self, queue):
        super(Node, self).__init__()
        self.queue = queue
        self.val = 1
        # mp.spawn(Node.do_something)
        # start_processes
        # do_something_out(self)
        self.x = Value(c_int, 1, lock=False)
        print(type(self.x))
        print("actual value:", self.x.value)
        mp.start_processes(
            do_something_out, args=(self, self.queue, self.x), start_method="fork"
        )

    def do_something(self):
        # t.queue.put("do_something")
        print("do_something")
        self.val = 2


if __name__ == "__main__":
    queue = mp.Queue()
    # queue.put("test")
    node = Node(queue)
    # mp.start_processes(
    #    do_something_out,
    #    (node, queue,),
    #    start_method="fork"
    # )

    time.sleep(3)
    print(node.queue.get())
    print(node.val)
    print(node.x.value)
    # print(node.val)
