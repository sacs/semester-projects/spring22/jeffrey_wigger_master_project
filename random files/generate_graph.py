import matplotlib.pyplot as plt
import networkx as nx

from decentralizepy.graphs.Regular import Regular
from decentralizepy.graphs.Ring import Ring
from decentralizepy.graphs.Star import Star

# b = Regular(16, 1, 686)


b = Regular(96*3, 5)
# TODO: rewrite to directly connect dissconnected subgraphs
# b.connect_graph()

b.write_graph_to_file(f"{96*3}_regular.edges")

g = nx.read_edgelist(f"{96*3}_regular.edges")
nx.draw(g)
#plt.savefig("96_star.png")
