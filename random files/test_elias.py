import numpy as np
import time
import os, random, pickle, lzma, leb128, lz4.frame
import uvarint

# arr = np.random.poisson(3, 500000) #np.random.randint(1, 20, 500000, dtype=np.int32)
# arr=arr.astype(np.int32)
# print("got the array")
# t5 = time.time()
#
#
# lzfour= lz4.frame.compress(arr)
#
# p = pickle.dumps(lzfour)
# print(len(p), len(lzfour))
# t6 = time.time()
#
# p = pickle.dumps(p)
#
# t7 = time.time()
# print(t6 -t5, t7 -t5)
#
# # elias implementation: taken from this stack overflow post:
# # https://stackoverflow.com/questions/62843156/python-fast-compression-of-large-amount-of-numbers-with-elias-gamma
# def encode(a):
#     a = a.view(f'u{a.itemsize}')
#     l = np.log2(a).astype('u1')
#     L = ((l<<1)+1).cumsum()
#     print("L,", L, len(L))
#     out = np.zeros(L[-1] + np.array([64], dtype = "u1")[0], 'u1')
#     print("lmax", l.max()+1)
#     for i in range(l.max()+1):
#         out[L-i-1] += (a>>i)&1
#     print("out:", out, out[-10:])
#     s = np.array([out.size], dtype=np.int64)
#
#     print(s)
#     size = np.ndarray(8, dtype='u1', buffer=s.data)
#     print("size:", size)
#     # out[-8:] = size
#     ss = s[0]
#     # for i in range(64):
#     #     out[int(L[-1] + 63 - i)] += (ss>>i)&1
#     print("out", out, out[-68:])
#
#     # out.data.contiguous
#     packed = np.packbits(out)
#     packed[-8:] = size
#     print("packed:", packed[-10:])
#     print("out", out, out[-68:])
#     return packed, out.size
#
# def decode(b,n):
#     print(b,)
#     n_arr = b[-8:]
#     print("n_arr:", n_arr)
#     n = np.ndarray(1, dtype=np.int64, buffer=n_arr.data)[0]
#     print("n:", n)
#     b = b[:-8]
#     b = np.unpackbits(b,count=n).view(bool)
#     s = b.nonzero()[0]
#     s = (s<<1).repeat(np.diff(s,prepend=-1))
#     s -= np.arange(-1,len(s)-1)
#     s = s.tolist() # list has faster __getitem__
#     ns = len(s)
#     def gen():
#         idx = 0
#         yield idx
#         while idx < ns:
#             idx = s[idx]
#             yield idx
#     offs = np.fromiter(gen(),int)
#     sz = np.diff(offs)>>1
#     mx = sz.max()+1
#     out = np.zeros(offs.size-1,int)
#     for i in range(mx):
#         out[b[offs[1:]-i-1] & (sz>=i)] += 1<<i
#     return out
#
# arr = np.random.poisson(3, 500000) + 1 # elias does not work on 0s # .frame.decompress
# arr= arr.astype(np.int64)
# print("arr:", arr)
#
#
# # t0 = time.time()
# # p = pickle.dumps(arr)
# # t1 = time.time()
# #
# # z = lzma.compress(arr)
# #
# # t2 = time.time()
# #
# # # From https://stackoverflow.com/questions/68968796/variable-length-integer-encoding
# # leb = b''.join(map(leb128.LEB128U.encode, arr))
# # t3 = time.time()
# #
# # uvar = b''.join(map(uvarint.encode, arr))
# t4 = time.time()
#
# elias, n = encode(arr)
#
# t5 = time.time()
# #
# # lzfour= lz4.frame.compress(arr.tobytes("C"))
# #
# # t6 = time.time()
#
# # print(elias)
# # print(n)
# # print(elias.size)
# # print(elias.itemsize)
# # print("array size'd:", f'{ arr.size * arr.itemsize:,}')
# # print("pickle'd:", f'{len(p):,}')
# # print("lzma'd:", f'{len(z):,}')
# # print("leb128'd:", f'{len(leb):,}')
# # print("uvarint'd:", f'{len(leb):,}')
# # print("elias'd:", f'{elias.size:,}')
# # print("elias'd:", f'{len(elias):,}')
# # print("elias'd:", f'{len(pickle.dumps(elias)):,}')
# # print("lz4'd:", f'{len(lzfour):,}')
# # print(f"pickle: {t1-t0:.5f}s, lzma: {t2-t1:.5f}s, leb128 {t3-t2:.5f}s, uvarint: {t4-t3:.5f}s, elias: {t5-t4:.5f}s, lz4: {t6-t5:.5f}s")
#
#
#
# # decode
# d0 = time.time()
#
# arr_dec = decode(elias, n)
# print(arr.dtype, arr_dec.dtype)
# print(len(arr) , len(arr_dec))
# assert len(arr) == len(arr_dec)
# assert (arr == arr_dec).all()
#
# d1 = time.time()
#
# # arr_dec = lz4.frame.decompress(lzfour)
# # arr_dec = np.frombuffer(arr_dec, dtype=np.int32)
# d2 = time.time()
# # print(arr_dec)
#
# print(d1 -d0, d2 - d1)


from decentralizepy.compression.Elias import Elias

arr = np.random.poisson(30, 50000) + 1 # elias does not work on 0s # .frame.decompress
arr= arr.astype(np.int32)
arr = np.cumsum(arr).astype(np.int32)
print("arr:", arr[:3], arr[-3:])

#print("Elias of framework:", arr, np.diff(arr), len(np.diff(arr)))
eliaso = Elias()
t1 = time.time()
comp = eliaso.compress(arr)
t2 = time.time()
print("size:", comp.dtype, len(comp), len(pickle.dumps(arr)), len(pickle.dumps(comp)))
arr_dec = eliaso.decompress(comp)
t3= time.time()
print(t2-t1, t3-t2, t3-t1)
print("arr_dec:", arr_dec[:3], arr_dec[-3:])
print(arr.dtype, arr_dec.dtype)
print(len(arr) , len(arr_dec))
assert len(arr) == len(arr_dec)
assert (arr == arr_dec).all()


arr = np.array([0,1,2,8])

comp = eliaso.compress(arr)
arr_dec = eliaso.decompress(comp)
print("test for fix", arr, arr_dec)
print(arr_dec)
print(arr.dtype, arr_dec.dtype)
print(len(arr) , len(arr_dec))
assert len(arr) == len(arr_dec)
assert (arr == arr_dec).all()

