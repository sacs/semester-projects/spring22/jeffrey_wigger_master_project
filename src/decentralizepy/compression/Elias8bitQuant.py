# elias implementation: taken from this stack overflow post:
# https://stackoverflow.com/questions/62843156/python-fast-compression-of-large-amount-of-numbers-with-elias-gamma
import fpzip
import numpy as np
import torch

from decentralizepy.compression.Elias import Elias
from torch.quantization.observer import MinMaxObserver, MovingAverageMinMaxObserver, HistogramObserver
from torch.nn.quantized.modules.utils import _quantize_weight

class Elias8bitQuant(Elias):
    """
    Compression API

    """

    def __init__(self):
        """
        Constructor
        """

    def compress_float(self, arr):
        """
        compression function for float arrays

        Parameters
        ----------
        arr : np.ndarray
            Data to compress

        Returns
        -------
        bytearray
            encoded data as bytes

        """
        scheme = torch.per_tensor_affine
        observer = MovingAverageMinMaxObserver(
            qscheme=scheme)
        # MinMaxObserver, MovingAverageMinMaxObserver(qscheme=scheme), HistogramObserver(qscheme=scheme)
        arr = torch.from_numpy(arr)
        observer.forward(arr)
        o = observer.calculate_qparams()
        aq = torch.quantize_per_tensor(
            arr,
            o[0].item(), o[1].item(), torch.quint8)
        return aq


    def decompress_float(self, bytes_array):
        """
        decompression function for compressed float arrays

        Parameters
        ----------
        bytes_array :bytearray
            compressed data

        Returns
        -------
        arr : np.ndarray
            decompressed data as array

        """

        def convert(aq):
            return (aq.int_repr().to(torch.float32) - aq.q_zero_point()) * aq.q_scale()
        return convert(bytes_array).numpy().astype(np.float32)
