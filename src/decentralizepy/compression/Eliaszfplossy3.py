import numpy as np

from decentralizepy.compression.Elias import Elias
from pyzfp import compress, decompress

class Eliaszfplossy3(Elias):
    """
    Compression API

    """

    def __init__(self):
        """
        Constructor
        """

    def compress_float(self, arr):
        """
        compression function for float arrays

        Parameters
        ----------
        arr : np.ndarray
            Data to compress

        Returns
        -------
        bytearray
            encoded data as bytes

        """
        return (np.array(compress(arr, tolerance=0.005, parallel=False)), arr.shape, arr.dtype)

    def decompress_float(self, bytes_array):
        """
        decompression function for compressed float arrays

        Parameters
        ----------
        bytes_array :bytearray
            compressed data

        Returns
        -------
        arr : np.ndarray
            decompressed data as array

        """
        return decompress(bytes_array[0], bytes_array[1], bytes_array[2], tolerance=0.005)
