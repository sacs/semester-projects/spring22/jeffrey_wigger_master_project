# elias implementation: taken from this stack overflow post:
# https://stackoverflow.com/questions/62843156/python-fast-compression-of-large-amount-of-numbers-with-elias-gamma
import fpzip
import numpy as np
import torch

from decentralizepy.compression.Elias import Elias


class Elias16f(Elias):
    """
    Compression API

    """

    def __init__(self):
        """
        Constructor
        """

    def compress_float(self, arr):
        """
        compression function for float arrays

        Parameters
        ----------
        arr : np.ndarray
            Data to compress

        Returns
        -------
        bytearray
            encoded data as bytes

        """
        return arr.astype(np.float16)


    def decompress_float(self, bytes_array):
        """
        decompression function for compressed float arrays

        Parameters
        ----------
        bytes_array :bytearray
            compressed data

        Returns
        -------
        arr : np.ndarray
            decompressed data as array

        """
        return bytes_array.astype(np.float32)
