# elias implementation: taken from this stack overflow post:
# https://stackoverflow.com/questions/62843156/python-fast-compression-of-large-amount-of-numbers-with-elias-gamma
import numpy as np

from decentralizepy.compression.Compression import Compression
import fastvarints

class Elias(Compression):
    """
    Compression API

    """

    def __init__(self):
        """
        Constructor
        """

    def compress(self, arr):
        """
        compression function

        Parameters
        ----------
        arr : np.ndarray
            Data to compress

        Returns
        -------
        bytearray
            encoded data as bytes

        """
        arr.sort()
        first = arr[0]
        arr = np.diff(arr, prepend=[first]).astype(np.int32) # prepend is just used to get the right size array
        arr[0] = first + 1
        packed = fastvarints.compress(arr)
        return packed

    def decompress(self, bytes_array):
        """
        decompression function

        Parameters
        ----------
        bytes_array :bytearray
            compressed data

        Returns
        -------
        arr : np.ndarray
            decompressed data as array

        """
        prepDifference = fastvarints.decompress(bytes_array)
        prepDifference[0] -= 1
        out = np.cumsum(prepDifference).astype(np.int64) # It return np.uint64, which is not supported by torch
        return out

    def compress_float(self, arr):
        """
        compression function for float arrays

        Parameters
        ----------
        arr : np.ndarray
            Data to compress

        Returns
        -------
        bytearray
            encoded data as bytes

        """
        return arr

    def decompress_float(self, bytes_array):
        """
        decompression function for compressed float arrays

        Parameters
        ----------
        bytes_array :bytearray
            compressed data

        Returns
        -------
        arr : np.ndarray
            decompressed data as array

        """
        return bytes_array
