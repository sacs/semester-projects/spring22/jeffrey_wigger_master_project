import logging
import weakref
from collections import deque

import numpy as np
import torch
import torch.multiprocessing as mp

from decentralizepy.sharing.DPSGDRW import DPSGDRW


class SharingWithRWAsync(DPSGDRW):
    """
    This implementation of Sharing with RW does synchronized rounds with its neighbour. On top of that it runs
    additional RW steps that are asynchronously sent at the _send_rw.
    This implementation will only work together with TCPRandomWalk.

    """

    def __init__(
        self,
        rank,
        machine_id,
        communication,
        mapping,
        graph,
        model,
        dataset,
        log_dir,
        rw_chance=0.1,
        rw_length=4,
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Local rank
        machine_id : int
            Global machine id
        communication : decentralizepy.communication.Communication
            Communication module used to send and receive messages
        mapping : decentralizepy.mappings.Mapping
            Mapping (rank, machine_id) -> uid
        graph : decentralizepy.graphs.Graph
            Graph reprensenting neighbors
        model : decentralizepy.models.Model
            Model to train
        dataset : decentralizepy.datasets.Dataset
            Dataset for sharing data. Not implemented yet! TODO
        log_dir : str
            Location to write shared_params (only writing for 2 procs per machine)

        """
        super().__init__(
            rank,
            machine_id,
            communication,
            mapping,
            graph,
            model,
            dataset,
            log_dir,
            rw_chance,
            rw_length,
        )

    def _averaging(self):
        """
        Averages the received model with the local model

        """
        with torch.no_grad():
            total = dict()
            weight_total = 0
            batch = self._preprocessing_received_models()

            for n, vals in batch.items():
                if len(vals) > 1:
                    data = None
                    degree = 0
                    # this should no longer happen, unless we get two rw from the same originator
                    logging.info("averaging double messages for %i", n)
                    for val in vals:
                        degree_sub, iteration, data_sub = val
                        if data is None:
                            data = data_sub
                            degree = degree
                        else:
                            for key, weight_val in data_sub.items():
                                data[key] += weight_val
                            degree = max(degree, degree_sub)
                    for key, weight_val in data.items():
                        data[key] /= len(vals)
                else:
                    degree, iteration, data = vals[0]

                weight = 1 / (max(len(batch), degree) + 1)  # Sort of Metro-Hastings-ish
                weight_total += weight
                for key, value in data.items():
                    if key in total:
                        total[key] += value * weight
                    else:
                        total[key] = value * weight

            for key, value in self.model.state_dict().items():
                total[key] += (1 - weight_total) * value  # Metro-Hastings


        self.model.load_state_dict(total)
        to_cat = []
        for _, v in self.model.state_dict().items():
            vf = v.clone().detach().flatten()
            to_cat.append(vf)
        self.init_model = torch.cat(to_cat)
        self.new_model = total

    def step(self):
        """
        Perform a sharing step. Implements D-PSGD.

        """
        self._pre_step()
        data = self.serialized_model()
        my_uid = self.mapping.get_uid(self.rank, self.machine_id)
        all_neighbors = self.graph.neighbors(my_uid)
        iter_neighbors = self.get_neighbors(all_neighbors)
        data["degree"] = len(all_neighbors)
        data["iteration"] = self.communication_round
        encrypted = self.communication.encrypt(data)
        for i, neighbor in enumerate(iter_neighbors):
            logging.debug("sending in SharedWithRWAsync to %i", neighbor)
            self.communication.send(neighbor, encrypted, False)

        logging.info("Waiting for messages from neighbors")
        while not self.received_from_all():
            sender, data = self.communication.receive()
            logging.debug("Received model from {}".format(sender))
            if data.get("rw", False):
                self.current_rw_messages.setdefault(sender, []).append(data)
                logging.info(
                    "Deserialized received rw from {} of iteration {}".format(
                        sender, data["iteration"]
                    )
                )
            else:
                self.peer_deques[sender].append(data)
                logging.info(
                    "Deserialized received model from {} of iteration {}".format(
                        sender, data["iteration"]
                    )
                )

        logging.info("Starting model averaging after receiving from all neighbors")
        self._averaging()
        logging.info("Model averaging complete")

        self._send_rw()
        self._post_step()
        self.communication_round += 1

    def __del__(self):
        logging.info(
            "Node had on average %f rw messages. And %f double visits",
            np.mean(self.rw_messages_stat),
            np.mean(self.rw_double_count_stat),
        )
