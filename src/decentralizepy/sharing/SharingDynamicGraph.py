import logging
import weakref
from collections import deque

import numpy as np
import torch
import torch.multiprocessing as mp

from decentralizepy.sharing.DPSGDRW import DPSGDRW


class SharingDynamicGraph(DPSGDRW):
    """
    This implementation of Sharing uses RW to sample a new neighbour to connect to. The graph is dynamic. The neighbours
    are bound to neighbor_bound = (min, max) However, it might temporary exceed the max.
    This implementation will only work together with TCPRandomWalkRouting.

    """

    def __init__(
        self,
        rank,
        machine_id,
        communication,
        mapping,
        graph,
        model,
        dataset,
        log_dir,
        rw_chance=1,
        rw_length=6,
        neighbor_bound=(3, 5),
        change_topo_interval=10,
        avg = False
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Local rank
        machine_id : int
            Global machine id
        communication : decentralizepy.communication.Communication
            Communication module used to send and receive messages
        mapping : decentralizepy.mappings.Mapping
            Mapping (rank, machine_id) -> uid
        graph : decentralizepy.graphs.Graph
            Graph reprensenting neighbors
        model : decentralizepy.models.Model
            Model to train
        dataset : decentralizepy.datasets.Dataset
            Dataset for sharing data. Not implemented yet! TODO
        log_dir : str
            Location to write shared_params (only writing for 2 procs per machine)
        change_topo_interval : int
            there is a big if it is set to 1, because there is special code for round 0

        """
        super().__init__(
            rank,
            machine_id,
            communication,
            mapping,
            graph,
            model,
            dataset,
            log_dir,
            rw_chance,
            rw_length,
        )
        self.avg = avg
        self.neighbor_bound_lower = neighbor_bound[0]
        self.neighbor_bound_higher = neighbor_bound[1]
        self.change_topo_interval = change_topo_interval

    def reconnect_rw(self):
        """
        Find a new neighbor via a random walk on the topology

        """

        def send():
            rw_data = {
                "rw": True,
                "routing_info": self.uid,
                "degree": self.number_of_neighbors,
                "iteration": self.communication_round,
                "visited": [self.uid],
                "fuel": self.rw_length - 1,
            }
            logging.info("new rw message")
            self.communication.send("rw", rw_data)

        rw_chance = self.rw_chance
        while rw_chance >= 1.0:
            # TODO: make sure they are not sent to the same neighbour
            send()
            rw_chance -= 1
        rw_now = torch.rand(size=(1,), generator=self.random_generator).item()
        if rw_now < rw_chance:
            send()

    def _preprocessing_received_models(self):
        with torch.no_grad():
            batch = {}

            def process_data(data, src):
                degree = data["degree"]
                iteration = data["iteration"]
                assert iteration == self.communication_round
                logging.debug(
                    "Averaging model from neighbor {} of iteration {}".format(
                        src, iteration
                    )
                )
                data_dict = self.deserialized_model(data)
                model_data = data_dict
                batch.setdefault(src, []).append((degree, iteration, model_data))

            for i, n in enumerate(self.data_batch):
                data = self.data_batch[n]
                process_data(data, n)

            return batch

    def _averaging(self):
        """
        Averages the received model with the local model

        """
        with torch.no_grad():
            total = dict()
            weight_total = 0
            batch = self._preprocessing_received_models()

            for n, vals in batch.items():
                if len(vals) > 1:
                    data = None
                    degree = 0
                    # this should no longer happen, unless we get two rw from the same originator
                    logging.info("averaging double messages for %i", n)
                    for val in vals:
                        degree_sub, iteration, data_sub = val
                        if data is None:
                            data = data_sub
                            degree = degree
                        else:
                            for key, weight_val in data_sub.items():
                                data[key] += weight_val
                            degree = max(degree, degree_sub)
                    for key, weight_val in data.items():
                        data[key] /= len(vals)
                else:
                    degree, iteration, data = vals[0]

                if self.avg:
                    weight = 1 / (len(batch) + 1)
                else:
                    weight = 1 / (max(len(batch), degree) + 1)  # Sort of Metro-Hastings-ish
                weight_total += weight
                for key, value in data.items():
                    if key in total:
                        total[key] += value * weight
                    else:
                        total[key] = value * weight

            for key, value in self.model.state_dict().items():
                total[key] += (1 - weight_total) * value  # Metro-Hastings

        self.model.load_state_dict(total)
        self.new_model = total

    def step(self):
        """
        Perform a sharing step. Implements D-PSGD.

        """
        logging.info("--- COMMUNICATION ROUND {} ---".format(self.communication_round))
        self._pre_step()
        data = self.serialized_model()
        my_uid = self.mapping.get_uid(self.rank, self.machine_id)
        if (
            self.communication_round % self.change_topo_interval == 0
            and self.communication_round != 0
        ):
            pass

        all_neighbors = self.graph.neighbors(my_uid)
        iter_neighbors = self.get_neighbors(all_neighbors)
        data["degree"] = len(all_neighbors)
        data["iteration"] = self.communication_round

        self.communication.send(("all", self.communication_round), data)

        self.data_batch = self.communication.receive()

        logging.info("Starting model averaging after receiving from all neighbors")
        self._averaging()
        logging.info("Model averaging complete")


        self.communication_round += 1
        self._post_step()

        if self.communication_round % self.change_topo_interval == 0:
            logging.info(
                f"Establishing new connections by rw for Round {self.communication_round}"
            )
            self.reconnect_rw()
