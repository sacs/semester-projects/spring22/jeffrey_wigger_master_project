import logging
import weakref
from collections import deque

import numpy as np
import torch


class SharingWithRW:
    """
    This implementation of Sharing with RW does synchronized rounds with its neighbour. On top of that it sends
    RW messages in the same data package to a neighbour. Hence, this method does not work with encryption.
    A random walk message will arrive at its destination with a lag of rw_length - 1.
    This implementation is supposed to run with TCP.py

    """

    def __init__(
        self,
        rank,
        machine_id,
        communication,
        mapping,
        graph,
        model,
        dataset,
        log_dir,
        rw_chance=0.1,
        rw_length=4,
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Local rank
        machine_id : int
            Global machine id
        communication : decentralizepy.communication.Communication
            Communication module used to send and receive messages
        mapping : decentralizepy.mappings.Mapping
            Mapping (rank, machine_id) -> uid
        graph : decentralizepy.graphs.Graph
            Graph reprensenting neighbors
        model : decentralizepy.models.Model
            Model to train
        dataset : decentralizepy.datasets.Dataset
            Dataset for sharing data. Not implemented yet! TODO
        log_dir : str
            Location to write shared_params (only writing for 2 procs per machine)

        """
        self.rank = rank
        self.machine_id = machine_id
        self.uid = mapping.get_uid(rank, machine_id)
        self.communication = communication
        self.mapping = mapping
        self.graph = graph
        self.model = model
        self.dataset = dataset
        self.communication_round = 0
        self.log_dir = log_dir
        self.total_data = 0
        self.random_generator = torch.Generator()
        self.rw_seed = (
            self.random_generator.seed()
        )  # new random seed from the random device
        logging.info("Machine %i has random seed %i for RW", self.uid, self.rw_seed)
        self.rw_chance = rw_chance
        self.current_rw_messages = []
        self.rw_messages_stat = []
        self.rw_double_count_stat = []
        self.rw_length = rw_length
        self.rw_sampler = weakref.WeakMethod(
            self.rw_sampler_equi_check_history
        )  # self.rw_sampler_equi

        self.peer_deques = dict()
        self.neighbors = self.graph.neighbors(self.uid)
        self.number_of_neighbors = len(self.neighbors)
        for n in self.neighbors:
            self.peer_deques[n] = deque()

    def received_from_all(self):
        """
        Check if all neighbors have sent the current iteration

        Returns
        -------
        bool
            True if required data has been received, False otherwise

        """
        for _, i in self.peer_deques.items():
            if len(i) == 0:
                return False
        return True

    def get_neighbors(self, neighbors):
        """
        Choose which neighbors to share with

        Parameters
        ----------
        neighbors : list(int)
            List of all neighbors

        Returns
        -------
        list(int)
            Neighbors to share with

        """
        # modify neighbors here
        return neighbors

    def serialized_model(self):
        """
        Convert model to a dictionary. Here we can choose how much to share

        Returns
        -------
        dict
            Model converted to dict

        """
        m = dict()
        for key, val in self.model.state_dict().items():
            m[key] = val.clone().detach()
            self.total_data += len(self.communication.encrypt(m[key]))
        data = {"params": m}
        return data

    def deserialized_model(self, m):
        """
        Convert received dict to state_dict.

        Parameters
        ----------
        m : dict
            received dict

        Returns
        -------
        state_dict
            state_dict of received

        """
        data = dict()
        data["params"] = m["params"]
        rw_data = m.get("rw", [])
        data["rw"] = rw_data
        return data

    def _pre_step(self):
        """
        Called at the beginning of step.

        """
        pass

    def _post_step(self):
        """
        Called at the end of step.

        """
        pass

    def _averaging(self):
        """
        Averages the received model with the local model

        """
        with torch.no_grad():
            total = dict()
            weight_total = 0
            batch = {}
            for i, n in enumerate(self.peer_deques):
                data = self.peer_deques[n].popleft()
                degree = data["degree"]
                iteration = data["iteration"]
                logging.debug(
                    "Averaging model from neighbor {} of iteration {}".format(
                        n, iteration
                    )
                )
                data_dict = self.deserialized_model(data)
                data = data_dict["params"]
                rw_data = data_dict["rw"]
                batch.setdefault(n, []).append((degree, iteration, data))
                second_visit = 0
                if len(rw_data) != 0:
                    for rw in rw_data:
                        og = rw["visited"][0]
                        if len(rw["visited"]) == 1 and og == n:
                            # assert len(rw["visited"]) == 1 # is a new rw message originiating from n
                            logging.info(
                                "RW message not double counted (%s)", str(rw["visited"])
                            )
                        elif self.uid in rw["visited"]:
                            logging.info(
                                "RW message was already once received (%s)",
                                str(rw["visited"]),
                            )
                            second_visit += 1
                        else:
                            batch.setdefault(og, []).append(
                                (rw["degree"], rw["iteration"], rw["params"])
                            )
                        # we do not use messages we already used, but we will forward it again in the next iteration
                        self.current_rw_messages.append(rw)
            self.rw_messages_stat.append(len(self.current_rw_messages))
            self.rw_double_count_stat.append(second_visit)
            for n, vals in batch.items():
                if len(vals) > 1:
                    data = None
                    degree = 0
                    logging.info("averaging double messages for %i", n)
                    for val in vals:
                        degree_sub, iteration, data_sub = val
                        if data is None:
                            data = data_sub
                            degree = degree
                        else:
                            for key, weight_val in data_sub.items():
                                data[key] += weight_val
                            degree = max(degree, degree_sub)
                    for key, weight_val in data.items():
                        data[key] /= len(vals)
                else:
                    degree, iteration, data = vals[0]

                weight = 1 / (max(len(batch), degree) + 1)  # Sort of Metro-Hastings-ish
                weight_total += weight
                for key, value in data.items():
                    if key in total:
                        total[key] += value * weight
                    else:
                        total[key] = value * weight

            for key, value in self.model.state_dict().items():
                total[key] += (1 - weight_total) * value  # Metro-Hastings

        self.model.load_state_dict(total)

    def step(self):
        """
        Perform a sharing step. Implements D-PSGD.

        """
        self._pre_step()
        data = self.serialized_model()
        my_uid = self.mapping.get_uid(self.rank, self.machine_id)
        all_neighbors = self.graph.neighbors(my_uid)
        iter_neighbors = self.get_neighbors(all_neighbors)
        data["degree"] = len(all_neighbors)
        data["iteration"] = self.communication_round

        rw_now = torch.rand(size=(1,), generator=self.random_generator).item()
        chosen_neighbor = None
        # The current implementation tracks the visited nodes, if it reaches that node again then the RW is ended.
        rw_neighbors = {}
        if len(self.current_rw_messages) != 0:
            for m in self.current_rw_messages:
                chosen_neighbor = self.rw_sampler()(
                    m
                )  # torch.randint(0, self.number_of_neighbors, size = (1,), generator = self.random_generator).item()
                visited = m["visited"]
                if (
                    m["fuel"] == 0 or chosen_neighbor == None
                ):  # for true random walk we do not do this condition: self.uid in visited
                    # we drop this message
                    logging.info("dropped rw message with fuel %i ", m["fuel"])
                else:
                    visited.append(self.uid)
                    m["visited"] = visited
                    m["fuel"] -= 1
                    rw_neighbors.setdefault(chosen_neighbor, []).append(m)
                    logging.info(
                        "rw message from %i to %i (%s)",
                        visited[0],
                        chosen_neighbor,
                        str(visited),
                    )
        self.current_rw_messages = []
        if rw_now < self.rw_chance:
            # will have to send the data twice to make the code simpler (for the beginning)
            rw_data = {
                "params": data["params"],
                "degree": self.number_of_neighbors,
                "iteration": self.communication_round,
                "visited": [self.uid],
                "fuel": self.rw_length - 1,
            }
            # chosen_neighbor should never be None here
            chosen_neighbor = self.rw_sampler()(None)
            assert chosen_neighbor is not None
            logging.info("new rw message")
            rw_neighbors.setdefault(chosen_neighbor, []).append(rw_data)

        for neighbor in iter_neighbors:
            if neighbor in rw_neighbors:
                data["rw"] = rw_neighbors[neighbor]  # is a list of random walk values
            self.communication.send(neighbor, data)

        logging.info("Waiting for messages from neighbors")
        while not self.received_from_all():
            sender, data = self.communication.receive()
            logging.debug("Received model from {}".format(sender))
            self.peer_deques[sender].append(data)
            logging.info(
                "Deserialized received model from {} of iteration {}".format(
                    sender, data["iteration"]
                )
            )

        logging.info("Starting model averaging after receiving from all neighbors")
        self._averaging()
        logging.info("Model averaging complete")

        self.communication_round += 1
        self._post_step()

    def rw_sampler_equi(self, message):

        index = torch.randint(
            0, len(self.neighbors), size=(1,), generator=self.random_generator
        ).item()
        logging.debug(
            "rw_sampler_equi selected index {} of {} {}".format(
                index, self.neighbors, type(self.neighbors)
            )
        )
        return list(self.neighbors)[index]

    def rw_sampler_equi_check_history(self, message):
        if message is None:  # RW starts from here
            index = torch.randint(
                0, len(self.neighbors), size=(1,), generator=self.random_generator
            ).item()
            logging.debug(
                "rw_sampler_equi_check_history selected index {} of {} {}".format(
                    index, self.neighbors, type(self.neighbors)
                )
            )
            return list(self.neighbors)[index]
        else:
            visited = set(message["visited"])
            neighbors = self.neighbors  # is already a set
            possible_neigbors = neighbors.difference(visited)
            if len(possible_neigbors) == 0:
                return None
            else:
                index = torch.randint(
                    0,
                    len(possible_neigbors),
                    size=(1,),
                    generator=self.random_generator,
                ).item()
                return list(possible_neigbors)[index]

    def rw_sampler_mh(self, message):
        # Metro hastings version of the sampler
        # Samples the neighbour based on the MH weights, allows self loops (will just decrease fuel and reroll!)
        pass

    def __del__(self):
        logging.info(
            "Node had on average %f rw messages. And %f double visits",
            np.mean(self.rw_messages_stat),
            np.mean(self.rw_double_count_stat),
        )
