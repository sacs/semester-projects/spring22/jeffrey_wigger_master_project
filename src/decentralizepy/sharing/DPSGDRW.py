import logging
import weakref
from collections import deque

import numpy as np
import torch
import torch.multiprocessing as mp

from decentralizepy.sharing.Sharing import Sharing


class DPSGDRW(Sharing):
    """
    Alternative implementation of DPSGD. Here, we send the weights before the gradients are calculated (at the beginning of the round, which is
    equivalent to it being at the end of the round).
    In _send_rw it also has the option to send RW messages.
    This implementation will only work together with TCPRandomWalk.


    """

    def __init__(
        self,
        rank,
        machine_id,
        communication,
        mapping,
        graph,
        model,
        dataset,
        log_dir,
        rw_chance=0.1,
        rw_length=4,
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Local rank
        machine_id : int
            Global machine id
        communication : decentralizepy.communication.Communication
            Communication module used to send and receive messages
        mapping : decentralizepy.mappings.Mapping
            Mapping (rank, machine_id) -> uid
        graph : decentralizepy.graphs.Graph
            Graph reprensenting neighbors
        model : decentralizepy.models.Model
            Model to train
        dataset : decentralizepy.datasets.Dataset
            Dataset for sharing data. Not implemented yet! TODO
        log_dir : str
            Location to write shared_params (only writing for 2 procs per machine)

        """
        super().__init__(
            rank, machine_id, communication, mapping, graph, model, dataset, log_dir
        )
        self.random_generator = torch.Generator()
        self.rw_seed = (
            self.random_generator.seed()
        )  # new random seed from the random device
        logging.info("Machine %i has random seed %i for RW", self.uid, self.rw_seed)
        self.rw_chance = rw_chance
        self.current_rw_messages = dict()
        self.rw_messages_stat = []
        self.rw_double_count_stat = []
        self.rw_length = rw_length


        self.number_of_neighbors = len(self.my_neighbors)

    def _send_rw(self):
        """
        Called at the end of step.

        """

        def send():
            # will have to send the data twice to make the code simpler (for the beginning)
            rw_data = {
                "params": self.init_model.numpy(),
                "rw": True,
                "degree": self.number_of_neighbors,
                "iteration": self.communication_round,
                "visited": [self.uid],
                "fuel": self.rw_length - 1,
            }
            logging.info("new rw message")
            self.communication.send(None, rw_data)

        rw_chance = self.rw_chance
        while rw_chance >= 1.0:
            # TODO: make sure they are not sent to the same neighbour
            send()
            rw_chance -= 1
        rw_now = torch.rand(size=(1,), generator=self.random_generator).item()
        if rw_now < rw_chance:
            send()

    def _preprocessing_received_models(self):
        with torch.no_grad():
            batch = {}
            self._second_visit = 0
            self._total_rw = 0

            def process_data(data, src):
                degree = data["degree"]
                iteration = data["iteration"]
                logging.debug(
                    "Averaging model from neighbor {} of iteration {}".format(
                        src, iteration
                    )
                )
                data_dict = self.deserialized_model(data)
                is_rw = data.get("rw", False)
                logging.debug(f"is rw {is_rw}")
                model_data = data_dict
                add = True
                if is_rw:
                    self._total_rw += 1
                    # if len(data["visited"]) == 1 and og == n:
                    if self.uid in data["visited"]:
                        logging.info(
                            "RW message was already once received (%s)",
                            str(data["visited"]),
                        )
                        self._second_visit += 1
                        add = False
                    elif src in self.my_neighbors:
                        # This message is from our direct neighbour from which we already got a newer weight
                        # So we are not using it
                        # assert len(rw["visited"]) == 1 # is a new rw message originiating from n
                        logging.info(
                            "RW message not double counted (%s)", str(data["visited"])
                        )
                        add = False
                    logging.info(
                        f"RW lag is: {self.communication_round - iteration}, for ({data['visited']}), added = {add}"
                    )
                if add:
                    batch.setdefault(src, []).append((degree, iteration, model_data))

            for i, n in enumerate(self.peer_deques):
                if len(self.peer_deques[n]) > 0:
                    data = self.peer_deques[n].popleft()
                    process_data(data, n)
            for k, val in self.current_rw_messages.items():
                for data in val:
                    process_data(data, k)
            # TODO: we delete the messages here in the first pass but _averaging does not use them in the first pass
            self.current_rw_messages = dict()  # [k] = deque()
            self.rw_messages_stat.append(self._total_rw)
            self.rw_double_count_stat.append(self._second_visit)

            return batch

    def _averaging(self):
        """
        Averages the received model with the local model

        """
        with torch.no_grad():
            total = dict()
            weight_total = 0
            if self.communication_round != 0:
                batch = self._preprocessing_received_models()
                for n, vals in batch.items():
                    if len(vals) > 1:
                        data = None
                        degree = 0
                        # this should no longer happen, unless we get two rw from the same originator
                        logging.info("averaging double messages for %i", n)
                        for val in vals:
                            degree_sub, iteration, data_sub = val
                            if data is None:
                                data = data_sub
                                degree = degree
                            else:
                                for key, weight_val in data_sub.items():
                                    data[key] += weight_val
                                degree = max(degree, degree_sub)
                        for key, weight_val in data.items():
                            data[key] /= len(vals)
                    else:
                        degree, iteration, data = vals[0]

                    weight = 1 / (
                        max(len(batch), degree) + 1
                    )  # Sort of Metro-Hastings-ish
                    weight_total += weight
                    for key, value in data.items():
                        if key in total:
                            total[key] += value * weight
                        else:
                            total[key] = value * weight

                if len(total) != 0:
                    init_dict = self.deserialized_model({"params": self.init_model.numpy()})
                    for (key, value) in init_dict.items():
                        # self.model.state_dict().items():
                        total[key] += (1 - weight_total) * value  # Metro-Hastings

                    # apply the update to the model
                    for key, value in self.model.state_dict().items():
                        # subtract the model change from the new average weight
                        total[key] = total[key] + (value - init_dict[key])
                    self.model.load_state_dict(total)
                else:
                    logging.debug("Node did not receive nothing")
            # The first round is completely a local update
            to_cat = []
            for _, v in self.model.state_dict().items():
                vf = v.clone().detach().flatten()
                to_cat.append(vf)
            self.init_model = torch.cat(to_cat)

    def step(self):
        """
        Perform a sharing step. Implements D-PSGD.

        """
        logging.info("--- COMMUNICATION ROUND {} ---".format(self.communication_round))
        self._pre_step()
        logging.info("Waiting for messages from neighbors")
        if self.communication_round != 0:
            while not self.received_from_all():
                sender, data = self.communication.receive()
                logging.debug("Received model from {}".format(sender))
                if data.get("rw", False):
                    self.current_rw_messages.setdefault(sender, []).append(data)
                    logging.info(
                        "Deserialized received rw from {} of iteration {}".format(
                            sender, data["iteration"]
                        )
                    )
                else:
                    # TODO: sort them by the iteration?
                    self.peer_deques[sender].append(data)
                    logging.info(
                        "Deserialized received model from {} of iteration {}".format(
                            sender, data["iteration"]
                        )
                    )

        logging.info("Starting model averaging after receiving from all neighbors")
        self._averaging()
        logging.info("Model averaging complete")

        self.communication_round += 1
        # Want to RW messages to be sent first
        # Breaks API
        self._post_step()

        # Needs to be before such that G_topK is set in TopK version
        data = self.serialized_model()
        self._send_rw()
        # For D-PSG we send the weights before the gradients are calculated (at the beginning of the round, which is
        # equivalent to it being at the end of the round)
        my_uid = self.mapping.get_uid(self.rank, self.machine_id)
        all_neighbors = self.graph.neighbors(my_uid)
        iter_neighbors = self.get_neighbors(all_neighbors)
        data["degree"] = len(all_neighbors)
        data["iteration"] = self.communication_round
        encrypted = self.communication.encrypt(data)
        for i, neighbor in enumerate(iter_neighbors):
            logging.debug("sending in DPSGDRWAsync to %i", neighbor)
            self.communication.send(neighbor, encrypted, False)

    def __del__(self):
        if len(self.rw_messages_stat) != 0 and len(self.rw_double_count_stat) != 0:
            logging.info(
                "Node had on average %f rw messages. And %f double visits",
                np.mean(self.rw_messages_stat),
                np.mean(self.rw_double_count_stat),
            )
