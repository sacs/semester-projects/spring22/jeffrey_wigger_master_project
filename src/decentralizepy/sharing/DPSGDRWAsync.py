import logging
import time
import weakref
from collections import deque

import numpy as np
import torch
import torch.multiprocessing as mp

from decentralizepy.sharing.DPSGDRW import DPSGDRW


class DPSGDRWAsync(DPSGDRW):
    """
     Alternative implementation of DPSGD. It is completely Async and based on DPSGDRW.
     rw_chance is used to set the number of neighbours to which a RW is sent. It should be bigger than 1.
     If rw_length is set to 1. Then it is like async DPSGD.
     This implementation will only work together with TCPRandomWalk.

    """

    def __init__(
        self,
        rank,
        machine_id,
        communication,
        mapping,
        graph,
        model,
        dataset,
        log_dir,
        rw_chance=1,
        rw_length=4,
        comm_interval=0.5,
        min_interval=0.001,
        max_lag=2,
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Local rank
        machine_id : int
            Global machine id
        communication : decentralizepy.communication.Communication
            Communication module used to send and receive messages
        mapping : decentralizepy.mappings.Mapping
            Mapping (rank, machine_id) -> uid
        graph : decentralizepy.graphs.Graph
            Graph reprensenting neighbors
        model : decentralizepy.models.Model
            Model to train
        dataset : decentralizepy.datasets.Dataset
            Dataset for sharing data. Not implemented yet! TODO
        log_dir : str
            Location to write shared_params (only writing for 2 procs per machine)

        """
        super().__init__(
            rank,
            machine_id,
            communication,
            mapping,
            graph,
            model,
            dataset,
            log_dir,
            rw_chance,
            rw_length,
        )
        self.comm_interval = comm_interval
        self.comm_interval_original = comm_interval
        self.delta_interval = comm_interval / 1000
        self.delta_interval_neutral = -comm_interval / 10000
        self.max_lag = max_lag
        self.min_interval = min_interval

        # TODO: log them
        self.comm_interval_history = []
        self.received_messages_history = []

    def _preprocessing_received_models(self):
        with torch.no_grad():
            batch = {}
            self._second_visit = 0
            self._total_rw = 0

            def process_data(data, src):
                degree = data["degree"]
                iteration = data["iteration"]
                logging.debug(
                    "Averaging model from neighbor {} of iteration {}".format(
                        src, iteration
                    )
                )
                data_dict = self.deserialized_model(data)
                is_rw = data.get("rw", False)
                logging.debug(f"is rw {is_rw}")
                model_data = data_dict
                add = True
                if is_rw:
                    self._total_rw += 1
                    # if len(data["visited"]) == 1 and og == n:
                    if self.uid in data["visited"]:
                        logging.info(
                            "RW message was already once received (%s)",
                            str(data["visited"]),
                        )
                        self._second_visit += 1
                        add = False
                    # elif src in self.my_neighbors: not desired in this async case
                    # TODO: but the message might be older than another one processed from that neighbour
                    logging.info(
                        f"RW lag is: {self.communication_round - iteration}, for ({data['visited']}), added = {add}"
                    )
                if add:
                    batch.setdefault(src, []).append((degree, iteration, model_data))

            for i, n in enumerate(self.peer_deques):
                if len(self.peer_deques[n]) > 0:
                    data = self.peer_deques[n].popleft()
                    process_data(data, n)
            for k, val in self.current_rw_messages.items():
                for data in val:
                    process_data(data, k)
            # TODO: we delete the messages here in the first pass but _averaging does not use them in the first pass
            self.current_rw_messages = dict()  # [k] = deque()
            self.rw_messages_stat.append(self._total_rw)
            self.rw_double_count_stat.append(self._second_visit)

            return batch

    def _averaging(self):
        """
        Averages the received model with the local model

        """
        with torch.no_grad():
            total = dict()
            weight_total = 0
            if self.communication_round != 0:
                batch = self._preprocessing_received_models()
                for n, vals in batch.items():
                    if len(vals) > 1:
                        data = None
                        degree = 0
                        # this should no longer happen, unless we get two rw from the same originator
                        logging.info("averaging double messages for %i", n)
                        for val in vals:
                            degree_sub, iteration, data_sub = val
                            if data is None:
                                data = data_sub
                                degree = degree
                            else:
                                for key, weight_val in data_sub.items():
                                    data[key] += weight_val
                                degree = max(degree, degree_sub)
                        for key, weight_val in data.items():
                            data[key] /= len(vals)
                    else:
                        degree, iteration, data = vals[0]

                    weight = 1 / (len(batch) + 1) # Not Metro_Hastings
                    weight_total += weight
                    for key, value in data.items():
                        if key in total:
                            total[key] += value * weight
                        else:
                            total[key] = value * weight

                if len(total) != 0:
                    #init_dict = self.deserialized_model({"params": self.init_model.numpy()})
                    for (key, value) in self.model.state_dict().items(): #in init_dict.items():
                        # self.model.state_dict().items():
                        total[key] += (1 - weight_total) * value  # Metro-Hastings

                    # apply the update to the model
                    #for key, value in self.model.state_dict().items():
                        # subtract the model change from the new average weight
                    #    total[key] = total[key] + (value - init_dict[key])
                    self.model.load_state_dict(total)
                else:
                    logging.debug("Node did not receive nothing")
            # The first round is completely a local update
            to_cat = []
            for _, v in self.model.state_dict().items():
                vf = v.clone().detach().flatten()
                to_cat.append(vf)
            self.init_model = torch.cat(to_cat)

    def step(self):
        """
        Perform a sharing step. Implements D-PSGD.

        """
        logging.info(
            "--- COMMUNICATION ROUND {} --- {} ---".format(
                self.communication_round, self.comm_interval
            )
        )
        self._pre_step()
        logging.info("Waiting for messages from neighbors")
        t_start = time.time()
        recv_messages = 0
        self.comm_interval_history.append(self.comm_interval)
        if self.communication_round != 0:
            while (time.time() - t_start) < self.comm_interval:
                # not self.received_from_all():
                sender, data = self.communication.receive(block=False, timeout=0.0001)
                if sender is None:
                    continue
                logging.debug("Received model from {}".format(sender))
                if data.get("rw", False):
                    self.current_rw_messages.setdefault(sender, []).append(data)
                    logging.info(
                        "Deserialized received rw from {} of iteration {}".format(
                            sender, data["iteration"]
                        )
                    )
                    # TODO: how to treat the case when no message is received
                    lag = self.communication_round - data["iteration"]
                    time_delta = (
                        self.delta_interval_neutral
                        if lag == 0
                        else lag * self.delta_interval
                    )
                    self.comm_interval += time_delta
                    self.comm_interval = max(self.comm_interval, self.min_interval)
                    recv_messages += 1
                else:
                    logging.info("Received normal RW message")

        self.received_messages_history.append(recv_messages)
        logging.info(
            "Step received %i in interval %f", recv_messages, self.comm_interval
        )
        self._averaging()
        logging.info("Model averaging complete")

        self.communication_round += 1
        self._post_step()
        # Sending RW message at the very end
        self._send_rw()

    def __del__(self):
        logging.info(
            "Node had on average %f rw messages. And %f double visits",
            np.mean(self.rw_messages_stat),
            np.mean(self.rw_double_count_stat),
        )
        logging.info(f"The comm intervalls were {self.comm_interval_history}")
        logging.info(
            f"Received messages history {np.mean(self.received_messages_history)}"
        )
